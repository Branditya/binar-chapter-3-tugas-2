package id.branditya.chapter3binartugas2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import id.branditya.chapter3binartugas2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setViewPager()

        binding.navigation.setOnItemSelectedListener {
            when(it.itemId){
                R.id.menu1 -> {
                    binding.viewPager.currentItem = 0
                }
                R.id.menu2 -> {
                    binding.viewPager.currentItem = 1
                }
            }
            true
        }
    }

    private fun setViewPager(){
        val listFragment = mutableListOf<Fragment>(
            FragmentOne(),
            FragmentTwo()
        )

        val viewPagerAdapter = ViewPagerAdapter(listFragment,supportFragmentManager,lifecycle)
        binding.viewPager.adapter = viewPagerAdapter

        val callback = object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                binding.navigation.menu.getItem(position).isChecked=true
            }
        }
        binding.viewPager.registerOnPageChangeCallback(callback)
    }
}